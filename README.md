# B738-MCP

738自动驾驶仪按键及旋钮模型。

<img src="/image/output/all.png" style="zoom:50%;"/>



### 更新记录

`2020.12.15` `Ver 1.0`

1. 初始版本

`2021.01.08 ` `Ver1.1`

1. COURSE直径修改为16mm，增减防滑纹密度，修正顶部图示。
2. DISENGAGE开关柄安装槽上移0.5mm。
3. ALTITUDE直径修改为16mm，增减防滑纹密度，高度修正为20mm。
4. HEADING底部翻转角度设定扭修改为直径20mm，顶部航向设定扭直径修改为15mm，指示针由两端指针修改为单短指针。
5. IAS修改为直径修改为16mm，防滑纹修改为三角防滑纹，顶部过渡弧修正了直径和角度。

`2021.02.01 ` `Ver1.2`

1. 修复了HEADING半轴孔内部有阻挡问题。
2. 所有旋钮加深半轴孔至15mm，底部开槽7mm以贴合面板并容纳螺母等。



### 待做事项

- [x] 2个磁航向
- [x] 1个转向度
- [x] 1个航向
- [x] 1个速度
- [x] 1个高度
- [x] 1个上升率下降率
- [x] 1个断自动驾驶
- [ ] 转向度和航向分为两个



